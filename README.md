# KSLOS
Repo ini dibuat untuk memepermudah pemberian materi KSLOS OmahTI

## Tempat main CTF
- [CTFtime](https://ctftime.org): All category
- [Crackme](https://crackmes.one/): PWN
- [PWNable.tw](https://pwnable.tw/)
- [PWNable.kr](https://pwnable.kr/)
- [CTFSme](https://tfs.me): All category
- [PWNable](http://pwnable.kr): Binary Exploit
- [Embedded Security CFT](https://microcorruption.com): Binary Exploit/Reverse Enginering
- [OverTheWire](http://overthewire.org): All category
- [PICOCTF17](https://2017game.picoctf.com)/[PICOCTF18](https://picoctf.com): All category
- [NaCTF](https://www.nactf.com)

## Materi
- [Modul ASGama](MATERI)
- [Channel CyberSecurityIPB](https://www.youtube.com/channel/UCH6CPf10u9uQu3w1DRhOliw)

## Tools
* **All Category**
    - Python and python-pip
    - PwnTools
    - NetCat (nc)

* **Cryptography**
    - John The Ripper
    - [Python pycrypto](https://pypi.org/project/pycrypto/)
    - sagemath   
    - [Rumkin](http://rumkin.com)
    - [quipqiup](https://quipqiup.com)
    - [alpetron](https://www.alpertron.com.ar/ECM.HTM)
    - [factordb](factordb.com)

* **Binary Exploit/Reverse Enginering**
    - Gdb-Peda
    - Ghidra
    - Binary Ninja
    - Hopper ori
    - Radare
    - ida free / idapro original

* **Forensic**
    - Audacity
    - Wireshark
    - Bless (hex editor)
    - Foremost
    - Binwalk
    - Volatility
    - Stegsolve
    - Exifool
    - python pil library
    - qrtool

* **Web Exploitation**
    - Python request module
    - dirbuster
    - sqlmap
    - https://www.ssllabs.com/ssltest/
    - EditThisCookie
    - Burp Suite
